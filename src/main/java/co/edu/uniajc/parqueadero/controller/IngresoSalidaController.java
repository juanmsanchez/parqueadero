package co.edu.uniajc.parqueadero.controller;

import co.edu.uniajc.parqueadero.model.IngresoSalidaVehiculoModel;
import co.edu.uniajc.parqueadero.service.IngresoSalidaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/salida")
@Api("salidas")
public class IngresoSalidaController {

    private IngresoSalidaService ingresoSalidaService;

    @Autowired
    public IngresoSalidaController(IngresoSalidaService ingresoSalidaService) {
        this.ingresoSalidaService = ingresoSalidaService;
    }

    @PostMapping(path = "/entry")
    @ApiOperation(value = "Generate entry vehiculo", response = IngresoSalidaVehiculoModel.class)
    public IngresoSalidaVehiculoModel generateEntry(@RequestBody IngresoSalidaVehiculoModel ingresoVehiculo){
        return ingresoSalidaService.generateEntryVehiculo(ingresoVehiculo);
    }

    @PutMapping (path = "/out")
    @ApiOperation(value = "Generate out vehiculo", response = IngresoSalidaVehiculoModel.class)
    public void generateOut (@RequestParam(name = "placa") String placa,@RequestParam(name = "login") String login){
        try {
            ingresoSalidaService.generateOutVehiculo(placa,login);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}