package co.edu.uniajc.parqueadero.controller;

import co.edu.uniajc.parqueadero.model.VehiculoModel;
import co.edu.uniajc.parqueadero.service.VehiculoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/vehiculo")
@Api("vehiculos")
public class VehiculoController {

    private VehiculoService vehiculoService;

    @Autowired
    public VehiculoController(VehiculoService vehiculoService) {
        this.vehiculoService = vehiculoService;
    }

    @PostMapping(path =  "/save")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "OK"),
            @io.swagger.annotations.ApiResponse(code = 400, message = "ERROR API"),
            @ApiResponse(code = 500, message = "ERROR SERVER")
    })
    public VehiculoModel saveVehiculo (@RequestBody VehiculoModel vehiculoModel){
        return vehiculoService.createVehiculo(vehiculoModel);
    }


    @PutMapping(path = "/update")
    @ApiOperation(value = "update vehiculo", response = VehiculoModel.class)
    public VehiculoModel updateVehiculo(@RequestBody VehiculoModel vehiculoModel){
        return vehiculoService.updateVehiculo(vehiculoModel);
    }


    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete vehiculo por ID", response = VehiculoModel.class)
    public void deletevehiculo(@RequestParam(name = "id") long id){
        vehiculoService.deleteVehiculo(id);
    }


    @GetMapping(path = "/findAll")
    @ApiOperation(value = "Find vehiculo All", response = VehiculoModel.class)
    public List<VehiculoModel> findAll(){
        return vehiculoService.findVehiculo();
    }

}
