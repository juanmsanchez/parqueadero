package co.edu.uniajc.parqueadero.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "ingreso_salida_vehiculo")
public class IngresoSalidaVehiculoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "isv_id")
    private Long isv_id;

    @Column (name = "isv_fecha_ingreso")
    private Date isv_fecha_ingreso;

    @Column (name = "isv_hora_ingreso")
    private Time isv_hora_ingreso;

    @Column (name = "isv_fecha_salida")
    private Date isv_fecha_salida;

    @Column (name = "isv_hora_salida")
    private Time isv_hora_salida;

    @Column (name = "isv_valor_cobrado")
    private BigDecimal isv_valor_cobrado;

    @Column (name = "isv_estado")
    private Boolean isv_estado;

    @Column (name = "isv_fecha_actualizacion")
    private Timestamp isv_fecha_actualizacion;

    @Column (name = "fk_isv_vehiculo")
    private long vehiculo;

    @Column (name = "fk_isv_usr")
    private long usuario;

    public IngresoSalidaVehiculoModel(Long isv_id, Date isv_fecha_ingreso, Time isv_hora_ingreso, Date isv_fecha_salida, Time isv_hora_salida, BigDecimal isv_valor_cobrado, Boolean isv_estado, Timestamp isv_fecha_actualizacion, long vehiculo, long usuario) {
        this.isv_id = isv_id;
        this.isv_fecha_ingreso = isv_fecha_ingreso;
        this.isv_hora_ingreso = isv_hora_ingreso;
        this.isv_fecha_salida = isv_fecha_salida;
        this.isv_hora_salida = isv_hora_salida;
        this.isv_valor_cobrado = isv_valor_cobrado;
        this.isv_estado = isv_estado;
        this.isv_fecha_actualizacion = isv_fecha_actualizacion;
        this.vehiculo = vehiculo;
        this.usuario = usuario;
    }
}


