package co.edu.uniajc.parqueadero.model;


import lombok.*;
import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "vehiculo")
public class VehiculoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name =  "v_id")
    private long v_id;

    @Column(name = "v_placa")
    private String v_placa;

    @Column(name = "v_nom_propietario")
    private String v_nom_propietario;

    @Column(name = "v_ape_propietario")
    private String v_ape_propietario;

    @Column(name = "v_id_propietario")
    private String v_id_propietario;

    @Column(name = "v_tel_propietario")
    private long v_tel_propietario;

    public VehiculoModel(long v_id, String v_placa, String v_nom_propietario, String v_ape_propietario, String v_id_propietario, long v_tel_propietario) {
        this.v_id = v_id;
        this.v_placa = v_placa;
        this.v_nom_propietario = v_nom_propietario;
        this.v_ape_propietario = v_ape_propietario;
        this.v_id_propietario = v_id_propietario;
        this.v_tel_propietario = v_tel_propietario;
    }
}
