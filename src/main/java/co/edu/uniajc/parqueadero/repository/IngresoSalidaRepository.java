package co.edu.uniajc.parqueadero.repository;

import co.edu.uniajc.parqueadero.model.IngresoSalidaVehiculoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Repository
public interface IngresoSalidaRepository extends JpaRepository<IngresoSalidaVehiculoModel, Long> {

    LocalDateTime dateTime = LocalDateTime.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    String datetime = dateTime.format(formatter);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value =
            "UPDATE ingreso_salida_vehiculo SET " +
            "isv_fecha_salida = (CURRENT_DATE)," +
            "isv_hora_salida = (CURRENT_TIME)," +
            "isv_estado = 'false'," +
            "isv_fecha_actualizacion = (CURRENT_TIMESTAMP)," +
            "fk_isv_usr = (SELECT usr_id FROM usuarios WHERE usr_login = :login) " +
            "WHERE " +
            "fk_isv_vehiculo = (SELECT v_id FROM vehiculo WHERE v_placa = :placa) " +
            "AND fk_isv_usr = (SELECT usr_id FROM usuarios WHERE usr_login = :login) " +
            "AND isv_estado = 'true' " +
            "AND isv_fecha_actualizacion = (SELECT MAX(isv_fecha_actualizacion) FROM ingreso_salida_vehiculo)")
    void generateOutVehiculo (@Param(value = "placa")String placa,@Param(value = "login")String login);

    @Query(nativeQuery = true, value =
            "SELECT isv_estado "+
            "FROM ingreso_salida_vehiculo "+
            "WHERE "+
            "fk_isv_vehiculo = (SELECT v_id FROM vehiculo WHERE v_placa = :placa) "+
            "AND isv_fecha_actualizacion = (SELECT MAX(isv_fecha_actualizacion) FROM ingreso_salida_vehiculo)")
    String findState (@Param(value = "placa")String placa);

    /*SELECT
    date_part('hour',isv_hora_salida-isv_hora_ingreso)*1500 as horas,
    date_part('minute',isv_hora_salida-isv_hora_ingreso)*25 as minutos
    from ingreso_salida_vehiculo;*/

}
