package co.edu.uniajc.parqueadero.repository;


import co.edu.uniajc.parqueadero.model.VehiculoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehiculoRepository extends JpaRepository<VehiculoModel, Long> {

    @Query(nativeQuery = true, value =
            "SELECT" +
                    "v_id" +
                    "v_placa" +
                    "v_nom_propietario" +
                    "v_ape_propietario" +
                    "v_id_propietario" +
                    "v_tel_propietario" +
                    "FROM vehiculo" +
                    "WHERE v_placa= :placa")
    List<VehiculoModel> findByPlaca (@Param(value = "placa")String placa);

}
