package co.edu.uniajc.parqueadero.service;

import co.edu.uniajc.parqueadero.model.IngresoSalidaVehiculoModel;
import co.edu.uniajc.parqueadero.repository.IngresoSalidaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngresoSalidaService {

    private final IngresoSalidaRepository ingresoSalidaRepository;

    @Autowired
    public IngresoSalidaService(IngresoSalidaRepository ingresoSalidaRepository) {
        this.ingresoSalidaRepository = ingresoSalidaRepository;
    }

    public IngresoSalidaVehiculoModel generateEntryVehiculo (IngresoSalidaVehiculoModel ingresoSalidaVehiculoModel){
        return ingresoSalidaRepository.save(ingresoSalidaVehiculoModel);
    }

    public void generateOutVehiculo (String placa,String login){

        if (ingresoSalidaRepository.findState(placa) == "true") {
            ingresoSalidaRepository.generateOutVehiculo(placa,login);
        }
        else {
            throw new RuntimeException(" El Vehiculo ya salio del parqueadero, estado = false");
            //System.out.print("Vehiculo ya salio del parqueadero, estado = false");
        }

    }
}
