package co.edu.uniajc.parqueadero.service;


import co.edu.uniajc.parqueadero.model.VehiculoModel;
import co.edu.uniajc.parqueadero.repository.VehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehiculoService {

    private final VehiculoRepository vehiculoRepository;

    @Autowired
    // LLamar en constructor el obj CrearRepository para que al momento de llamar la clase se cargue de manera automatica.
    public VehiculoService(VehiculoRepository vehiculoRepository) {
        this.vehiculoRepository = vehiculoRepository;
    }

    // Create
    public VehiculoModel createVehiculo (VehiculoModel vehiculoModel) {

        return vehiculoRepository.save(vehiculoModel);
    }

    // Actualiza
    public VehiculoModel updateVehiculo (VehiculoModel vehiculoModel) {

        return vehiculoRepository.save(vehiculoModel);
    }

    //Consultar
    public List<VehiculoModel> findVehiculo (){
        return vehiculoRepository.findAll();
    }

    // Eliminar
    public  void deleteVehiculo (Long id){
        vehiculoRepository.deleteById(id);
    }

}
