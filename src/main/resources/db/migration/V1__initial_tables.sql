CREATE TABLE "vehiculo"
(
	v_id serial NOT NULL,
	v_placa character varying(10) NOT NULL,
	v_nom_propietario character varying(50),
	v_ape_propietario character varying(50),
	v_id_propietario character varying(20),
	v_tel_propietario bigint,
	PRIMARY KEY (v_id)
);

ALTER TABLE "vehiculo" OWNER to postgres;


CREATE TABLE "usuarios"
(
	usr_id serial NOT NULL,
	usr_login character varying(50) NOT NULL,
	usr_pass character varying(50) NOT NULL,
	usr_state boolean NOT NULL,
	PRIMARY KEY (usr_id)
);

ALTER TABLE "usuarios" OWNER to postgres;


CREATE TABLE "ingreso_salida_vehiculo"
(
    isv_id serial NOT NULL,
    isv_fecha_ingreso date NOT NULL,
    isv_hora_ingreso time NOT NULL,
    isv_fecha_salida date,
    isv_hora_salida time,
    isv_valor_cobrado money,
    isv_estado boolean NOT NULL,
    isv_fecha_actualizacion timestamp NOT NULL,
    fk_isv_usr serial NOT NULL,
    fk_isv_vehiculo serial NOT NULL,
    PRIMARY KEY (isv_id),
    CONSTRAINT fk_isv_usr FOREIGN KEY (fk_isv_usr)
        REFERENCES public.usuarios (usr_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_isv_vehiculo FOREIGN KEY (fk_isv_vehiculo)
        REFERENCES public.vehiculo (v_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE "ingreso_salida_vehiculo" OWNER to postgres;